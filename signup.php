<?php
	/* Andrey Aleksandrov 0447608 */
	
	$response->result = -1;

	if(!isset($_POST['uname']) || !isset($_POST['pwd'])){
		//Password or username missing
		header("Location: signupform.php");
		die();
	}

	$svname = "localhost";
	$uname = "root";
	$pwd = "password";
	$db = "todolist";

	// Database access --------------------------
	$conn = new PDO("mysql:host=$svname;dbname=$db", $uname, $pwd);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


	$check = $conn->prepare('SELECT uName FROM users WHERE uName = :uname');
	$check->bindParam(":uname", $_POST['uname']);
	$check->execute();
	if (!empty($check->fetchAll())){
		//username taken
		$response->result = 1;
		echo json_encode($response);
		die();
	}
	//-------------------------------------
	$hash = password_hash($_POST['pwd'], PASSWORD_DEFAULT);
	//print_r($hash);

	$stmt = $conn->prepare('INSERT INTO users(uName, pHash) VALUES (:u, :h)');

	$stmt->bindParam(':u', $_POST['uname']);
	$stmt->bindParam(':h', $hash);
	$stmt->execute(); 

	$response->result = 0;
	echo json_encode($response);
	die();
?>
