<?php
	/* Andrey Aleksandrov 0447608 */
	$response->result = -1;

	if (session_status() == PHP_SESSION_NONE) {
    	session_start();
	}

	if(!isset($_SESSION['uID']) || !isset($_SESSION['username'])) {
		//Session not set
		//Not allowed to fetch notes
		$response->result = 2;
		$response->sessID = $_SESSION['uID'];
		$response->sessName = $_SESSION['username'];
		echo json_encode($response);
		die();
	}

	//Database access ------------------------
	$svname = "localhost";
	$uname = "root";
	$pwd = "password";
	$db = "todolist";

	$conn = new PDO("mysql:host=$svname;dbname=$db", $uname, $pwd);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$stmt = $conn->prepare("SELECT id, note FROM notes WHERE uID = :id");
	$stmt->bindParam(':id', $_SESSION['uID']);
	$stmt->execute();
	$rs = $stmt->fetchAll(PDO::FETCH_ASSOC);
	//---------------------------------------------
	$response->notes = $rs;

	$response->result = 0;
	echo json_encode($response);
	die();

	/*
	foreach ($rs as $row){
		echo '<li id="'.$row['id'].'">'.htmlspecialchars($row['note']).'</li>';
	}

	*/
?>
