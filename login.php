<?php
	/* Andrey Aleksandrov 0447608 */
	
	$response->result = -1;

	if(isset($_GET['logout'])){
		session_start();
		session_destroy();
		$response->result = 0;
		echo json_encode($response);
		die();
	}

	//Try to resume session
	if(isset($_GET['sess'])){
		session_start();
		
		if(isset($_SESSION['uID']) && isset($_SESSION['username'])) {
			//Valid session, log user back in
			$response->result = 0;
			$response->uID = $_SESSION['uID'];
			$response->uName = $_SESSION['username'];

			echo json_encode($response);
			die();
		} else {
			//Session invalid
			$response->result = -1;
			echo json_encode($response);
			die();
		}
	}

	if(!isset($_POST['uname']) || !isset($_POST['pwd'])){
		//Password or username missing
		$response->result = 
		die();
	}

	$svname = "localhost";
	$uname = "root";
	$pwd = "password";
	$db = "todolist";

	//Database access -----------------------
	$conn = new PDO("mysql:host=$svname;dbname=$db", $uname, $pwd);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$hash = password_hash($_POST['pwd'], PASSWORD_DEFAULT);

	$stmt = $conn->prepare('SELECT uID, uName, pHash FROM users WHERE uName = :u');

	$stmt->bindParam(':u', $_POST['uname']);
	$stmt->execute();
	$rs = $stmt->fetchAll(PDO::FETCH_ASSOC);
	//-------------------------------------------

	if (password_verify($_POST['pwd'] ,$rs[0]['pHash'])){
		//Password and username match
		session_start();
		$_SESSION['uID'] = $rs[0]['uID'];
		$_SESSION['username'] = $_POST['uname'];
		$response->result = 0;
		$response->uID = $rs[0]['uID'];
		$response->uName = $_POST['uname'];
		echo json_encode($response);
		die();

	}else{
		//Wrong password or username
		$response->result = 1;
		echo json_encode($response);
		die();
	}

	die();
?>
