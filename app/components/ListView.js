//Andrey Aleksandrov
'use strict';

import React from 'react';
import ListItem from './ListItem.js'

export default class ListView extends React.Component {
	
	constructor(props) {
		super(props);
	}

	render() {
		//Build list of ListItem components with proper props
		const list = this.props.items.map((item) => {
			return <ListItem key={item.id} item={item} onDelete={this.props.onDelete}/>;
		});

		return (<ul>
					{list}
				</ul>);
	}

}
