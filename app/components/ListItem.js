//Andrey Aleksandrov 0447608
'use strict';

import React from 'react';

export default class ListItem extends React.Component {

	constructor(props) {
		super(props);
	}

	handleDelete() {
		//Call deletion function passed form parent
		this.props.onDelete(this.props.item.id);
	}

	render() {
		return (
				<li id={this.props.item.id}>
					<div  className="listItem" onClick={this.handleDelete.bind(this)}>
						<p>{this.props.item.note}</p>
					</div>
				</li>);
	}
}
