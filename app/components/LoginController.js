//Andrey Aleksandrov 0447608

'use strict';

import React from 'react';

export default class LoginController extends React.Component {

	constructor(props) {
		super(props);
		this.state = { action: 'login', validInput: true, errorMsg: null};
	}


	handleSubmitButton(event) {

		if (this.state.action == 'register') {
			//Check validity of fields only when registering
			if (!document.getElementById('uname').checkValidity() || !document.getElementById('pwd').checkValidity()){
				this.setState( {validInput: false} );
				return;
			} else if(!(this.state.validInput)){
				this.setState( {validInput: true} );
			}
		}

		const uname = document.getElementById('uname').value;
		const pwd = document.getElementById('pwd').value;

		if (this.state.action === 'login') {
			//Ajax call to login

			//Set POST data
			const formData = new FormData();
			formData.append('uname', uname);
			formData.append('pwd', pwd);

			fetch('/login.php', {
				method: 'post',
				body: formData,
				credentials: 'same-origin'
			}).then((response) => response.json()).then((data) => {
				console.log(data);

				if (data.result != 0) {
					//Wrong password
					this.setState( { errorMsg: "Invalid credentials" } );
					return;
				}

				if (!(this.state.errorMsg === null)) {
					this.setState( { errorMsg: null } );
				}
				//Set logged in state of App
				this.props.setSession( { uID: data.uID, uName: data.uName });
			});
		} else {	
			//Ajax call to register

			//Set POST data
			const formData = new FormData();
			formData.append('uname', uname);
			formData.append('pwd', pwd);

			fetch('/signup.php', {
				method: 'post',
				body: formData,
				credentials: 'same-origin'
			}).then((response) => response.json()).then((data) => {
				console.log(data);

				//Username taken
				if (data.result === 1) {
					this.setState( { errorMsg: "Username taken" } );
					return;
				}
				//Update message
				this.setState( { errorMsg: "Successfully registered", action: "login" } );

			});			


		}
		
	}


	//Toggle view between register/login
	toggleAction(event) {
		const newAction = (this.state.action === 'login' ? 'register' : 'login');
		this.setState( { action: newAction, errorMsg: null, validInput: true} );
	}

	render() {
		const title = (this.state.action === 'login' ? "Log in" : "Register");
		const toggle = (this.state.action === 'login' ? "New user" : "Existing user");

		return (
				<div className="loginControl">
					<p> {title} </p>
					<input id='uname' type="text" placeholder="Username" pattern=".{6,}"/><br/>
					<input id='pwd' type="password" placeholder="Password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"/><br/>
					<button onClick={this.handleSubmitButton.bind(this)}>{title}</button>
					<button onClick={this.toggleAction.bind(this)}>{toggle}</button>
					{!this.state.validInput ? <p style={{color: "red"}}>One or more invalid field</p> : null} <br/>
					<p style={{color: "red"}}>{this.state.errorMsg}</p>
				</div>
			);

	}
}
