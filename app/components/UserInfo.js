//Andrey Aleksandrov 0447608
'use strict';

import React from 'react';

export default class UserInfo extends React.Component {

	constructor(props) {
		super(props);
	}

	handleLogout(event) {
		//Logout current user

		fetch('/login.php?logout=1', {
			credentials: 'same-origin'
		})
		.then((response) => response.json())
		.then((data) => {
			this.props.setSession(null);
			//Set App state to null session
		});
	}

	render() {
		return (<div className="userinfo">
					<span style={{margin: '10px', fontsize: '1em'}}>Logged in as {this.props.session.uName}</span>
					<button onClick={this.handleLogout.bind(this)}>Logout</button>
				</div>);
	}

}
