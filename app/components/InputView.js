//Andrey Aleksandrov 0447608
'use strict';

import React from 'react';

export default class InputView extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = { submitError: false, validInput: true };
		this.onClickHandler = this.onClickHandler.bind(this);
	}

	keyPressHandler(event) {
		//Handle keypresses, trigger clickhandler if its Enter
		if (event.key === 'Enter') {
			this.onClickHandler();
		}
	}

	onClickHandler(event) {

		//Check that input field isnt empty
		if (document.getElementById('noteInput').value.length === 0) {
			this.setState( {validInput: false} );
			return;
		} else if (!this.state.validInput) {	
			this.setState( { validInput: true } );
		}

		//Set POST request data
		const formData = new FormData();
		formData.append('note', document.getElementById('noteInput').value);

		//Send AJAX request to submit note
		fetch('/submit.php', {
			method: 'post',
			credentials: 'same-origin',
			body: formData
		})
		.then((response) => response.json())
		.then((data) => {
			if(data.result != 0) {
				//Error occured in submit.php
				this.setState( { submitError: true } );
				return;
			} else if (this.state.submitError) {
				this.setState( {submitError: false} );
				return;
			}

			console.log(data);
			//Rerender parent
			this.props.updateParent();
		});

	}

	render() {

		return (<div>
					<div className="inputcontainer">
						<input id="noteInput" maxLength="100" type='text' placeholder='Enter note...' onKeyPress={this.keyPressHandler.bind(this)}/>
						<button onClick={this.onClickHandler} onKeyPress={this.keyPressHandler.bind(this)}> Submit </button>
					</div>
					<p style={{fontsize: '16px', margin: '10px'}}>Max. 100 characters</p>
					{this.state.submitError ? <span>Error occured while submitting note</span> : null}
				</div>);
	}

}
