/* Andrey Aleksandrov 0447608 */

'use strict';

import React from 'react';
import InputView from './InputView.js'
import ListController from './ListController.js'
import LoginController from './LoginController.js'
import UserInfo from './UserInfo.js';

export default class App extends React.Component {

	constructor(props) {
		super(props);
		this.state = { session: null };
	}

	setSession(s) {
		//Function to pass to children so state can be changed
		this.setState({session: s});
	}

	update() {
		this.childList.getNotes();
	}

	componentWillMount() {
		//Check if this browser session is already logged in, resume session if so
		fetch('/login.php?sess=1', {
			credentials: 'same-origin'
		})
		.then((response) => response.json())
		.then((data) => {
			console.log("Session continue: " + data.result);
			if (data.result == 0) {
				//If session found, set state accordingly
				this.setSession({ uID: data.uID, uName: data.uName });
			}
			//Else force to log in
		});
	}

	render() {

		if (this.state.session === null) {
			//Prompt user to log in if session not set
			return <LoginController setSession = {this.setSession.bind(this)}/>;
		}

		return (
				<div>
					<p style={{fontSize: '40px', margin: '20px'}}>ToDo-List</p>
					<UserInfo style={{margin:'10px'}} session={this.state.session} setSession={this.setSession.bind(this)}/>
					<InputView session={this.state.session} updateParent={this.update.bind(this)}/>
					<p style={{fontSize: '30px', margin: '20px 10px 10px 10px'}}>Notes: </p>
					<span style={{fontSize: '1em', margin: '10px'}}>Click notes to remove</span>
					<ListController session={this.state.session} ref={instance => { this.childList = instance; }}/>
				</div>
			);

	}
}
