//Andrey Aleksandrov 0447608
'use strict';

import React from 'react';
import ListView from './ListView.js'

export default class ListController extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = { items : [], error: false };
		this.getNotes.bind(this);
	}


	getNotes() {
		//Get user's notes from database with AJAX call
		fetch('/notes.php', {
			credentials: 'same-origin'
		})
		.then((response) => response.json())
		.then((data) => {
			//Error check
			if (data.result != 0) {
				this.setState( { error: true } );
				return;
			}
			//Set note state
			this.setState( { items: data.notes } ); 

			console.log(data);
		});
	}

	deleteNote(id) {
		//Delete clicked note

		const formData = new FormData();
		formData.append('id', id);

		//AJAX request
		fetch('/remove.php', {
			method: 'post',
			body: formData,
			credentials: 'same-origin'
		})
		.then((response) => response.json())
		.then((data) => {

			if (data.result != 0) {
				//error
				console.log(data);
				return;
			}

			this.getNotes();

			console.log(data);
		});
		console.log(id);
	}


	componentWillMount() {
		this.getNotes();
	}

	render() {
		return this.state.error ? <p>Error occurred while loading notes, refresh the page.</p> : <ListView items={this.state.items} onDelete={this.deleteNote.bind(this)}/>;
	}
}
