//Andrey Aleksandrov 0447608
'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App.js'

//Render App component
ReactDOM.render(<App />, document.getElementById('root'));
