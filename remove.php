<?php
	/* Andrey Aleksandrov 0447608 */
	$response->result = -1;
	session_start();

	if(!isset($_SESSION['uID']) || !isset($_SESSION['username'])){
		//Not logged in, not allowed to delete notes
		$response->result = 2;
		echo json_encode($response);
		die();	
	}
	// Database access -----------------------------
	$svname = "localhost";
	$uname = "root";
	$pwd = "password";
	$db = "todolist";
	try {
		$conn = new PDO("mysql:host=$svname;dbname=$db", $uname, $pwd);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$stmt = $conn->prepare("DELETE FROM notes WHERE ID=:id AND uID=:uid");

		$stmt->bindParam(':id', $_POST['id']);
		$stmt->bindParam(':uid', $_SESSION['uID']);
		
		$stmt->execute();

		$response->result = 0;
		echo json_encode($response);
		die();
	} catch(Exception $e) {
		$response->result = 1;
		$response->error = $e.getTrace;
		echo json_encode($response);
		die();
	}
?>
