<?php

	$response->result = -1;
	session_start();
	
	if (!isset($_SESSION['uID']) || !isset($_SESSION['username'])){
		//Not logged in, cannot add notes
		$response->result = 2;
		echo json_encode($response);
		die();
	}
	//-----------------
	$svname = "localhost";
	$uname = "root";
	$pwd = "password";
	$db = "todolist";
	//-----------------
	
	try{
		$conn = new PDO("mysql:host=$svname;dbname=$db", $uname, $pwd);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$stmt = $conn->prepare("INSERT INTO notes (uID, note) VALUES (:uid, :note)");
		$stmt->bindParam(':uid', $_SESSION['uID']);
		$stmt->bindParam(':note', $_POST['note']);
		$stmt->execute();

		//Return code 0 (OK)
		$response->result = 0;
		echo json_encode($response);
		die();
	} catch(Exception $e) {
		//Error case, return error code 1
		$response->result = 1;
		echo json_encode($response);
		die();
	}

?>
